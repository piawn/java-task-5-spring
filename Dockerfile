FROM openjdk:14
ADD target/JavaSpringTask5-0.0.1-SNAPSHOT.jar javaspringtask5.jar
ENTRYPOINT [ "java", "-jar", "/javaspringtask5.jar"]