# youTrack
youTrack is a application that gets information from a music database.
The application shows you 5 random artists, tracks and genres, and let you search for tracks.

## Docker Image deployed on Heroku
https://experis-piawn-youtrack-docker.herokuapp.com/

## API Endpoints

### `GET` Get all customers
Returns a minimal version of all customers. Will change endpoint later to get full version of customer at api/customers and add minimal version to api/customers/min.

    /api/customers/

### `POST` Add new customer
Posts a Customer to the database. supportRepId is set in the repository, so you can leave it blank.

    /api/customers/

### `PUT` Update existing customer
Takes in a Customer and updates all variables. Must be a matching id in the database.

    /api/customers/

### `GET` Countries by customers
Returns each country and the number of customers from that country.

    /api/customers/country

### `GET` Highest spenders
Returns all the customers and their total spending.

    /api/customers/spending/highest

### `GET` Customers' most popular genres
Return customers most popular genre(s). Must add customerId in path.
    /api/customers/{id}/popular/genre