package com.noroff.JavaSpringTask5.data_access;

import com.noroff.JavaSpringTask5.models.TrackSearch;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SearchRepository {

    private String URL = ConnectionHelper.URL;
    private Connection conn = null;

    public ArrayList<TrackSearch> searchForTrack(String searchterm){
        ArrayList<TrackSearch> tracks = new ArrayList<>();
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "SELECT t.Name TrackName, artist.Name ArtistName, album.Title AlbumTitle, g.Name Genre " +
                                    "FROM Track t " +
                                    "INNER JOIN Genre g on t.GenreId = g.GenreId " +
                                    "INNER JOIN Album album on t.AlbumId = album.AlbumId " +
                                    "INNER JOIN Artist artist on album.ArtistId = artist.ArtistId " +
                                    "WHERE LOWER(t.name) LIKE LOWER(?)");
            prep.setString(1, "%" + searchterm + "%");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                tracks.add( new TrackSearch(
                        set.getString("TrackName"),
                        set.getString("ArtistName"),
                        set.getString("AlbumTitle"),
                        set.getString("Genre")
                ));
            }
            System.out.println("Track search went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return tracks;
    }
}
