package com.noroff.JavaSpringTask5.data_access;

import com.noroff.JavaSpringTask5.models.Artist;
import com.noroff.JavaSpringTask5.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TrackRepository {

    private String URL = ConnectionHelper.URL;
    private Connection conn = null;

    public ArrayList<Track> getFiveRandomTracks(){
        ArrayList<Track> tracks = new ArrayList<>();
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT TrackId, Name FROM Track ORDER BY RANDOM() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                tracks.add( new Track(
                        set.getString("TrackId"),
                        set.getString("Name")
                ));
            }
            System.out.println("Get five random tracks went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return tracks;
    }
}
