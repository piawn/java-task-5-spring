package com.noroff.JavaSpringTask5.data_access;

import com.noroff.JavaSpringTask5.models.Genre;
import com.noroff.JavaSpringTask5.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class GenreRepository {
    private String URL = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";
    private Connection conn = null;

    public ArrayList<Genre> getFiveRandomGenres(){
        ArrayList<Genre> genres = new ArrayList<>();
        // ---
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT GenreId, Name FROM Genre ORDER BY RANDOM() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                genres.add( new Genre(
                        set.getString("GenreId"),
                        set.getString("Name")
                ));
            }
            System.out.println("Get five random genres went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return genres;
    }
}
