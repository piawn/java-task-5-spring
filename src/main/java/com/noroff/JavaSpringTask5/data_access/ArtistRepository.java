package com.noroff.JavaSpringTask5.data_access;

import com.noroff.JavaSpringTask5.models.Artist;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ArtistRepository {

    private String URL = ConnectionHelper.URL;
    private Connection conn = null;

    public ArrayList<Artist> getFiveRandomArtists(){
        ArrayList<Artist> artists = new ArrayList<>();
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "SELECT ArtistId, Name " +
                                    "FROM Artist " +
                                    "ORDER BY RANDOM() LIMIT 5");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                artists.add( new Artist(
                        set.getString("ArtistId"),
                        set.getString("Name")
                ));
            }
            System.out.println("Get five random artists went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return artists;
    }
}
