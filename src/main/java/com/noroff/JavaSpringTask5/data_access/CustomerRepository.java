package com.noroff.JavaSpringTask5.data_access;

import com.noroff.JavaSpringTask5.models.*;

import java.sql.*;
import java.util.ArrayList;

public class CustomerRepository {
    private String URL = ConnectionHelper.URL;
    private Connection conn = null;

    public ArrayList<CustomerMin> getAllCustomers(){
        ArrayList<CustomerMin> customers = new ArrayList<>();
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone " +
                            "FROM Customer");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                customers.add( new CustomerMin(
                        set.getInt("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getString("Country"),
                        set.getString("PostalCode"),
                        set.getString("Phone")
                ));
            }
            System.out.println("Get all customers went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return customers;
    }

    public boolean addCustomer(Customer customer){
        boolean success = false;
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement( "INSERT INTO Customer(CustomerId, FirstName, LastName, Company, Address, City, State, Country," +
                            " PostalCode, Phone, Fax, Email, SupportRepId)" +
                            " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

            prep.setInt(1, customer.getCustomerId());
            prep.setString(2, customer.getFirstName());
            prep.setString(3, customer.getLastName());
            prep.setString(4, customer.getCompany());
            prep.setString(5, customer.getAddress());
            prep.setString(6, customer.getCity());
            prep.setString(7, customer.getState());
            prep.setString(8, customer.getCountry());
            prep.setString(9, customer.getPostalCode());
            prep.setString(10, customer.getPhone());
            prep.setString(11, customer.getFax());
            prep.setString(12, customer.getEmail());
            prep.setInt(13, 1);

            int result = prep.executeUpdate();
            success = (result != 0); // if res = 1; true

            System.out.println("Added new customer to the database!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return success;
    }

    public boolean updateCustomer(Customer customer){
        boolean success = false;
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement( "UPDATE Customer SET CustomerId=?, FirstName=?, LastName=?, Company=?, Address=?, City=?, State=?, Country=?," +
                            " PostalCode=?, Phone=?, Fax=?, Email=?, SupportRepId=? " +
                            "WHERE CustomerId = ?");

            prep.setInt(1, customer.getCustomerId());
            prep.setString(2, customer.getFirstName());
            prep.setString(3, customer.getLastName());
            prep.setString(4, customer.getCompany());
            prep.setString(5, customer.getAddress());
            prep.setString(6, customer.getCity());
            prep.setString(7, customer.getState());
            prep.setString(8, customer.getCountry());
            prep.setString(9, customer.getPostalCode());
            prep.setString(10, customer.getPhone());
            prep.setString(11, customer.getFax());
            prep.setString(12, customer.getEmail());
            prep.setInt(13, customer.getSupportRepId());
            prep.setInt(14, customer.getCustomerId());

            int result = prep.executeUpdate();
            success = (result != 0);

            System.out.println("Updated customer in the database!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return success;
    }




    public ArrayList<CustomersByCountry> getCustomersByCountry(){
        ArrayList<CustomersByCountry> customerCountByCountry = new ArrayList<>();
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "SELECT Country, COUNT(*) " +
                                    "FROM Customer " +
                                    "GROUP BY Country " +
                                    "ORDER BY COUNT(*) DESC");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                customerCountByCountry.add( new CustomersByCountry(
                        set.getString("Country"),
                        set.getString("COUNT(*)")));
            }
            System.out.println("Get customer count by country went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return customerCountByCountry;
    }


    public ArrayList<CustomersBySpending> getHighestSpendingCustomers(){
        ArrayList<CustomersBySpending> highestSpendingCustomers = new ArrayList<>();
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement(
                            "SELECT c.CustomerId, c.FirstName, c.LastName, SUM(i.Total) Total " +
                                    "FROM Customer as c " +
                                    "INNER JOIN Invoice as i using(CustomerId) " +
                                    "GROUP BY i.CustomerId " +
                                    "ORDER BY Total DESC");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                highestSpendingCustomers.add( new CustomersBySpending(
                        set.getInt("CustomerId"),
                        set.getString("FirstName"),
                        set.getString("LastName"),
                        set.getInt("Total")));
            }
            System.out.println("Get highest spending customers went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return highestSpendingCustomers;
    }

    public ArrayList<PopularGenre> getCustomerMostPopularGenre(int id){
        ArrayList<PopularGenre> genres = new ArrayList<>();

        try{
            // connect
            conn = DriverManager.getConnection(URL);

            PreparedStatement prep = conn.prepareStatement(
                    "WITH favorite_genre_occurrence AS (" +
                            "SELECT MAX(occurrence) max_occurrence " +
                            "FROM (" +
                                "SELECT COUNT(t.GenreId) occurrence " +
                                "FROM Genre g " +
                                "INNER JOIN Track t on g.GenreId = t.GenreId " +
                                "INNER JOIN InvoiceLine il on t.TrackId = il.TrackId " +
                                "INNER JOIN Invoice i on il.InvoiceId = i.InvoiceId " +
                                "INNER JOIN Customer c on i.CustomerId = c.CustomerId  " +
                                "WHERE c.CustomerId=? " +
                                "GROUP BY g.GenreId)) "
                            +
                            "SELECT c.CustomerId, g.GenreId, g.Name, COUNT(g.GenreId) AS genreCount " +
                            "FROM Genre g " +
                            "INNER JOIN Track t on g.GenreId = t.GenreId " +
                            "INNER JOIN InvoiceLine il on t.TrackId = il.TrackId " +
                            "INNER JOIN Invoice i on il.InvoiceId = i.InvoiceId " +
                            "INNER JOIN Customer c on i.CustomerId = c.CustomerId  " +
                            "WHERE c.CustomerId = ? " +
                            "GROUP BY g.GenreId " +
                            "HAVING genreCount = (SELECT max_occurrence FROM favorite_genre_occurrence) ");


            prep.setInt(1,id);
            prep.setInt(2,id);
            ResultSet set = prep.executeQuery();
            while(set.next()){
                genres.add( new PopularGenre(
                        set.getInt("CustomerId"),
                        set.getInt("GenreId"),
                        set.getString("Name"),
                        set.getInt("genreCount")));
            }

            System.out.println("Get customers most popular genre went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return genres;
    }
}
