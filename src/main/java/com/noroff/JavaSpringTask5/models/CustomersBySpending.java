package com.noroff.JavaSpringTask5.models;

public class CustomersBySpending {
    private int customerId;
    private String firstName;
    private String lastName;
    private int totalSpending;

    public CustomersBySpending() {
    }

    public CustomersBySpending(int customerId, String firstName, String lastName, int totalSpending) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.totalSpending = totalSpending;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getTotalSpending() {
        return totalSpending;
    }

    public void setTotalSpending(int totalSpending) {
        this.totalSpending = totalSpending;
    }
}
