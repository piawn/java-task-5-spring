package com.noroff.JavaSpringTask5.models;

public class CustomersByCountry {
    private String country;
    private String customers;

    public CustomersByCountry(String country, String customers) {
        this.country = country;
        this.customers = customers;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCustomers() {
        return customers;
    }

    public void setCustomers(String customers) {
        this.customers = customers;
    }
}
