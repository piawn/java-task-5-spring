package com.noroff.JavaSpringTask5.models;

public class PopularGenre {
    private int customerId;
    private int genreId;
    private String name;
    private int occurrences;

    public PopularGenre() {
    }

    public PopularGenre(int customerId, int genreId, String name, int occurrences) {
        this.customerId = customerId;
        this.genreId = genreId;
        this.name = name;
        this.occurrences = occurrences;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(int occurrences) {
        this.occurrences = occurrences;
    }
}