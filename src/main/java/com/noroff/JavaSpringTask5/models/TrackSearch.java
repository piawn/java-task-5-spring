package com.noroff.JavaSpringTask5.models;

public class TrackSearch {
    private String trackName;
    private String Artist;
    private String album;
    private String genre;

    public TrackSearch(String trackName, String artist, String album, String genre) {
        this.trackName = trackName;
        Artist = artist;
        this.album = album;
        this.genre = genre;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getArtist() {
        return Artist;
    }

    public void setArtist(String artist) {
        Artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
