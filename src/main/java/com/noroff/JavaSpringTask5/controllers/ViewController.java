package com.noroff.JavaSpringTask5.controllers;

import com.noroff.JavaSpringTask5.data_access.*;
import com.noroff.JavaSpringTask5.models.TrackSearch;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
public class ViewController {

    private final ArtistRepository artistRep = new ArtistRepository();
    private final TrackRepository trackRep = new TrackRepository();
    private final GenreRepository genreRep = new GenreRepository();
    private final SearchRepository searchRep = new SearchRepository();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getRandomArtists(Model model){
        model.addAttribute("artists", artistRep.getFiveRandomArtists());
        model.addAttribute("tracks", trackRep.getFiveRandomTracks());
        model.addAttribute("genres", genreRep.getFiveRandomGenres());
        return "home";
    }

    // Redirects to home if search is empty.
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchForTracks(@RequestParam(required = false) String searchterm, Model model){
        boolean success = false;

        model.addAttribute("searchterm", searchterm);

        String trimSearch = searchterm.trim();
        if (trimSearch.equals("")){
            return "redirect:/";
        }
        ArrayList<TrackSearch> searchresults = searchRep.searchForTrack(trimSearch);

        if (searchresults.size() > 0){
            success = true;
        }

        model.addAttribute("success", success);
        model.addAttribute("results", searchresults);
        model.addAttribute("searchterm", trimSearch);

        return "search";
    }
}
