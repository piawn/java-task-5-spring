package com.noroff.JavaSpringTask5.controllers;

import com.noroff.JavaSpringTask5.data_access.CustomerRepository;
import com.noroff.JavaSpringTask5.models.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {

    CustomerRepository custRep = new CustomerRepository();

    // http://localhost:8080/api/customers
    @RequestMapping(value = "/api/customers", method = RequestMethod.GET)
    public ArrayList<CustomerMin> getAllCustomers() {
        return custRep.getAllCustomers();
    }

    // http://localhost:8080/api/customers
    @RequestMapping(value = "/api/customers", method = RequestMethod.POST)
    public boolean getAllCustomers(@RequestBody Customer customer) {
        return custRep.addCustomer(customer);
    }

    // http://localhost:8080/api/customers
    @RequestMapping(value = "/api/customers", method = RequestMethod.PUT)
    public boolean updateCustomer(@RequestBody Customer customer) {
        return custRep.updateCustomer(customer);
    }

    // http://localhost:8080/api/customers/country
    @RequestMapping(value = "/api/customers/country", method = RequestMethod.GET)
    public ArrayList<CustomersByCountry> getCustomersCountByCountry() {
        return custRep.getCustomersByCountry();
    }

    // http://localhost:8080/api/customers/spending/highest
    @RequestMapping(value = "/api/customers/spending/highest", method = RequestMethod.GET)
    public ArrayList<CustomersBySpending> getCustomersBySpending() {
        return custRep.getHighestSpendingCustomers();
    }

    // http://localhost:8080/api/customers/{id}/popular/genre
    @RequestMapping(value = "api/customers/{id}/popular/genre")
    public ArrayList<PopularGenre> getSpecificCustomersMostPopularGenre(@PathVariable int id) {
        return custRep.getCustomerMostPopularGenre(id);
    }
}
