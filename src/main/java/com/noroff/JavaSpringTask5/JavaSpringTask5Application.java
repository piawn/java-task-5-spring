package com.noroff.JavaSpringTask5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringTask5Application {

	public static void main(String[] args) {
		SpringApplication.run(JavaSpringTask5Application.class, args);
	}

}
